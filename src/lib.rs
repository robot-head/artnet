extern crate ascii;
extern crate bincode;
#[macro_use]
extern crate bitflags;
extern crate env_logger;
extern crate futures;
extern crate hwaddr;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate tokio_core;

use ascii::AsciiStr;
use bincode::{deserialize, serialize};
use futures::{Future, Sink, Stream};
use hwaddr::HwAddr;
use serde::Serialize;
use serde::Serializer;
use std::error::Error;
use std::io;
use std::net::SocketAddr;
use tokio_core::net::{UdpCodec, UdpSocket};
use tokio_core::reactor::Core;


/// Networking related module
pub mod net {
    /// ArtPoll and ArtPollReply.
    ///
    ///   * https://art-net.org.uk/structure/discovery-packets/
    pub mod discovery {

        use ascii::AsciiStr;
        use hwaddr::HwAddr;
        use serde::{Serialize,Serializer};
        use std::io;
        use std::net::SocketAddr;
        use tokio_core::net::UdpCodec;

        #[derive(Debug)]
        pub struct ArtPoll {
            unilateral: bool,
            debug_messages: bool,
            priority: u8,
        }


        impl Serialize for ArtPoll {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
                where
                    S: Serializer,
            {
                let artnet_str = AsciiStr::from_ascii("Art-Net").unwrap();
                serializer.serialize_bytes(artnet_str.as_bytes())
            }
        }

        struct ArtPollReply {
            ip_address: SocketAddr,
            port: u16,
            firmware_revision: u16,
            port_address: u16,
            oem_code: u16,
            ubea_version: bool,
            status_1: bool,
            esta_mfr: u8,
            short_name: [char; 18],
            long_name: [char; 64],
            node_report: [char; 64],
            num_ports: u8,
            port_types: [u8; 4],
            good_input: [u8; 4],
            good_output: [u8; 4],
            sw_in: char,
            sw_out: char,
            sw_vide: bool,
            sw_macro: bool,
            sw_remote: bool,
            style: bool,
            mac_addr: HwAddr,
            bind_ip: SocketAddr,
            bind_index: bool,
            status2: bool,
        }

//        pub struct ArtPollCodec;
//
//        impl UdpCodec for ArtPollCodec {
//            type In = (SocketAddr, Vec<u8>);
//            type Out = ArtPoll;
//
//            fn decode(&mut self, addr: &SocketAddr, buf: &[u8]) -> io::Result<Self::In> {
//                Ok((*addr, buf.to_vec()))
//            }
//
//            fn encode(&mut self, art_poll: Self::Out, into: &mut Vec<u8>) -> SocketAddr {
//                //into.extend(art_poll);
//                addr
//            }
//        }
    }
}


#[cfg(test)]
mod tests {
    use super::net::discovery::ArtPoll;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn artpoll_serializes() {
        let ap = ArtPoll {
            unilateral: false,
            debug_messages: true,
            priority: 5,
        };

        assert_eq!(2 + 2, 4);
    }
}
